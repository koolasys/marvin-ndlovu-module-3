import 'package:flutter/material.dart';

Map<int, Color> color = {
  0: const Color.fromARGB(255, 230, 81, 0),
  50: const Color.fromRGBO(255, 230, 81, 0.1),
  100: const Color.fromRGBO(255, 230, 81, 0.2),
  200: const Color.fromRGBO(255, 230, 81, 0.3),
  300: const Color.fromRGBO(255, 230, 81, 0.4),
  400: const Color.fromRGBO(255, 230, 81, 0.5),
  500: const Color.fromRGBO(255, 230, 81, 0.6),
  600: const Color.fromRGBO(255, 230, 81, 0.7),
  700: const Color.fromRGBO(255, 230, 81, 0.8),
  800: const Color.fromRGBO(255, 230, 81, 0.9),
  900: const Color.fromRGBO(255, 230, 81, 1),
};

MaterialColor orangeColor = MaterialColor(0xFFF57C00, color);
MaterialColor greyColor = MaterialColor(0xff212121, color);
MaterialColor greenColor = MaterialColor(0xFF43a047, color);
MaterialColor white70 = MaterialColor(0xb3ffffff, color);
MaterialColor blackGreyLight = MaterialColor(0x42000000, color);
MaterialColor blackGreyDark = MaterialColor(0xdd000000, color);
MaterialColor iconColor = MaterialColor(0xff5ac18e, color);
MaterialColor whiteColor = MaterialColor(0xffffffff, color);

//final MaterialColor themeColor = Color(0xffbb5a5a);
ThemeData defaultTheme = ThemeData(
  appBarTheme: AppBarTheme(
    backgroundColor: greenColor,
    foregroundColor: whiteColor,
    titleTextStyle: const TextStyle(
      color: Colors.white,
      fontSize: 30,
      fontWeight: FontWeight.bold,
    ),
    //color: greyColor,
  ),
  scaffoldBackgroundColor: greyColor,
  primarySwatch: orangeColor,
  primaryColor: orangeColor,
  //accentColor: greenColor,
  canvasColor: greenColor,
  hintColor: Colors.black38,
  shadowColor: Colors.black26,
  iconTheme: IconThemeData(color: iconColor),
  //olorScheme: Colors.pink,
  //fontFamily: "Chilanka",
  textTheme: TextTheme(
    headline1:
        TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: whiteColor),
    headline2:
        TextStyle(fontSize: 16, color: white70, fontWeight: FontWeight.bold),
    headline3:
        TextStyle(fontSize: 20, color: white70, fontWeight: FontWeight.bold),
    headline4: TextStyle(fontSize: 16, color: blackGreyDark),
    headline5:
        TextStyle(fontSize: 15, color: whiteColor, fontWeight: FontWeight.bold),
    headline6: TextStyle(
      color: whiteColor,
      fontWeight: FontWeight.w500,
      fontSize: 17,
    ),

    //button: eleva

    //titleLarge: TextStyle(fontSize: 100, color: Colors.white),
    //headline1:
    //a,
  ),
  //primaryColor: themeColor,
);

ButtonStyle buttonStyle = ButtonStyle(
  backgroundColor:
      MaterialStateProperty.resolveWith((Set<MaterialState> states) {
    if (states.contains(MaterialState.pressed)) {
      return orangeColor;
    }
    return greenColor;
  }),
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
          borderRadius: const BorderRadius.only(
              topRight: Radius.circular(28),
              bottomRight: Radius.circular(28),
              bottomLeft: Radius.circular(28),
              topLeft: Radius.circular(28)),
          side: BorderSide(color: Colors.grey[700]!))),
);
/// Elevated button style
//final ButtonStyle elevButtonStyle = Elevated.styleForm();
