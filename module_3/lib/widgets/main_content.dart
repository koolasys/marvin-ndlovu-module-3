import 'package:flutter/material.dart';
import 'package:bottom_bar/bottom_bar.dart';
import 'package:module_3/widgets/profile.dart';
import 'market_view.dart';
import 'search_page.dart';
import './some_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _currentPage = 0;
  final _pageController = PageController();

  Widget buildHome() {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        leading: const Icon(Icons.logo_dev),
        toolbarHeight: 50,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
          "Smarket",
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        actions: [
          IconButton(
              onPressed: () => print("Add icon Pressed!!!"),
              icon: Icon(Icons.add_circle, color: Colors.orange[700])),
          IconButton(
              onPressed: () => print("Messages pressed!!!"),
              icon: Icon(Icons.mail_sharp, color: Colors.orange[700])),
          IconButton(
              onPressed: () => print("Notifications pressed!!!"),
              icon: Icon(
                Icons.notifications,
                color: Colors.orange[700],
              ))
        ],
      ),
      body: Container(
          padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
          child: TheMarket() //Text("Thsi is home"),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: [
          buildHome(),
          const SearchPage(),
          Container(color: Colors.greenAccent.shade700),
          const Scaffold(backgroundColor: Colors.grey),
          const ProfilePage(),
        ],
        onPageChanged: (index) {
          setState(() => _currentPage = index);
        },
      ),
      bottomNavigationBar: BottomBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        height: 60,
        selectedIndex: _currentPage,
        onTap: (int index) {
          _pageController.jumpToPage(index);
          setState(() => _currentPage = index);
        },
        items: <BottomBarItem>[
          BottomBarItem(
            icon: Icon(
              Icons.home,
              color: Theme.of(context).primaryColor,
            ),
            title: const Text('Home'),
            activeColor: Theme.of(context).canvasColor,
          ),
          BottomBarItem(
              icon: Icon(Icons.search, color: Theme.of(context).primaryColor),
              title: const Text("Search"),
              activeColor: Theme.of(context).canvasColor),
          BottomBarItem(
            icon: Icon(Icons.shopping_basket_rounded,
                color: Theme.of(context).primaryColor),
            title: const Text('Orders'),
            activeColor: Theme.of(context).canvasColor,
          ),
          BottomBarItem(
            icon: Icon(Icons.favorite, color: Theme.of(context).primaryColor),
            title: const Text('Favorites'),
            activeColor: Theme.of(context).canvasColor,
            //darkActiveColor: Colors.green[600], // Optional
          ),
          BottomBarItem(
              icon:
                  Icon(Icons.person_pin, color: Theme.of(context).primaryColor),
              title: const Text("Profile"),
              activeColor: Theme.of(context).canvasColor),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const SomePage()));
        },
        child: Icon(
          Icons.add_circle_sharp,
          size: 60,
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
