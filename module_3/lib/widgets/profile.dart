import 'package:flutter/material.dart';
import 'package:module_3/data/user_data.dart';
import 'package:module_3/widgets/edit_profile.dart';
import './profile_widget.dart';
import '../data/user.dart';
import './statistics_widget.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    const user = UserPreferences.myUser;
    return Scaffold(
      //backgroundColor: Theme.of(context).iconTheme.color,
      //appBar: buildAppBar(context),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          const SizedBox(height: 30),
          ProfileWidget(
            imgPath: user.imgPath,
            onClicked: () async {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const EditProfile()));
            },
          ),
          const SizedBox(
            height: 20,
          ),
          buildName(context, user),
          const SizedBox(height: 20),
          const Statistics(),
          const SizedBox(height: 30),
          buildAbout(context, user),
        ],
      ),
    );
  }
}

/* Building App bar */
AppBar buildAppBar(BuildContext context) {
  return AppBar(
    title: const Text("Profile"),
    //leading: BackButton(),
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}

/// Name of the user widget
Widget buildName(BuildContext context, User user) {
  return Column(
    children: [
      Text(
        user.name + " " + user.surname,
        style: Theme.of(context).textTheme.headline2,
      ),
      const SizedBox(
        height: 20,
      ),
      Text(
        user.email,
        style: Theme.of(context).textTheme.headline5,
      )
    ],
  );
}

/// Description of the user
Widget buildAbout(BuildContext context, User user) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 40),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "About",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(height: 5),
        Text(user.about, style: Theme.of(context).textTheme.headline5)
      ],
    ),
  );
}
