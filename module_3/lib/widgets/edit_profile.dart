import 'package:flutter/material.dart';
import 'package:module_3/data/user.dart';
import 'package:module_3/data/user_data.dart';
import 'package:module_3/themes/theme.dart';
import 'package:module_3/widgets/profile_widget.dart';
import './profile_edit_widgets.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  User user = UserPreferences.myUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 23),
        physics: const BouncingScrollPhysics(),
        children: [
          const SizedBox(height: 15),
          ProfileWidget(
            imgPath: user.imgPath,
            onClicked: () async {},
            isEdit: true,
          ),
          ProfileTextInput(
            label: "Full Name",
            text: user.name + " " + user.surname,
            onChanged: (name) {},
            maxLines: 1,
          ),
          ProfileTextInput(
            label: "Email",
            text: user.email,
            onChanged: (name) {},
            maxLines: 1,
          ),
          ProfileTextInput(
            label: "About Me",
            text: user.about,
            onChanged: (name) {},
            maxLines: 6,
          ),
          saveEditButton(context),
        ],
      ),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    title: Text("Edit Profile"),
    leading: const BackButton(),
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}

Widget saveEditButton(BuildContext context) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 35),
    width: 300,
    child: ElevatedButton(
        onPressed: () => print("Saved!!!"),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Text(
            "Save Profile",
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        style: buttonStyle //
        ),
  );
}
