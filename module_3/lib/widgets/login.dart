import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'main_content.dart';
import 'signup.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

/*The state of the SignIn page*/
class _SignInPageState extends State<SignInPage> {
  bool isRememberMe = false;

  /*Building email widget*/
  Widget buildEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Email Address",
          style: Theme.of(context).textTheme.headline2,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 14),
                prefixIcon: Icon(
                  Icons.email,
                  color: Theme.of(context).iconTheme.color,
                ),
                hintText: "Email",
                hintStyle: TextStyle(color: Theme.of(context).hintColor)),
          ),
        )
      ],
    );
  }

  /*Building password widget*/
  Widget buildPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Password",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.only(top: 14),
                prefixIcon: Icon(
                  Icons.lock,
                  color: Theme.of(context).iconTheme.color,
                ),
                hintText: "Password",
                hintStyle: TextStyle(color: Theme.of(context).hintColor)),
          ),
        )
      ],
    );
  }

/*Building forgot password*/
  Widget buildForgotPassBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => print("Forgot password presses!"),
        // padding: EdgeInsets.only(right: 0),
        child: Text(
          "Forgot Password?",
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
    );
  }

  /*Builing remember me check box*/
  Widget buildRememberCb() {
    return Container(
      height: 20,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: isRememberMe,
              checkColor: Theme.of(context).canvasColor,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  isRememberMe = value!;
                });
              },
            ),
          ),
          Text(
            "Rememer Me",
            style: Theme.of(context).textTheme.headline2,
          )
        ],
      ),
    );
  }

  /*Building create account button*/
  Widget buildLoginBTN() {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 35),
        width: 300,
        child: ElevatedButton(
          onPressed: () => {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => const MainPage()))
            // Navigator.of(context).pop((context) => SignInPage())
          },
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Text(
              "Login",
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.resolveWith((Set<MaterialState> states) {
              if (states.contains(MaterialState.pressed)) {
                return Theme.of(context).primaryColor;
              }
              return Theme.of(context).canvasColor;
            }),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(28),
                        bottomRight: Radius.circular(28),
                        bottomLeft: Radius.circular(28),
                        topLeft: Radius.circular(28)),
                    side: BorderSide(color: Colors.grey[700]!))),
          ), //
        ));
  }

  /*Building sign up button*/
  Widget buildSignUpBTN() {
    return GestureDetector(
      onTap: () => {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const SignUpPage()))
      },
      child: RichText(
        text: TextSpan(children: [
          TextSpan(
              text: "Don't have an Account? ",
              style: Theme.of(context).textTheme.headline6),
          TextSpan(
              text: "Sign Up", style: Theme.of(context).textTheme.headline2),
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: const [
                      0.1,
                      0.9
                    ],
                        colors: [
                      Theme.of(context).scaffoldBackgroundColor,
                      Theme.of(context).primaryColor
                    ])),
                child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    padding: const EdgeInsets.fromLTRB(30, 120, 30, 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Sign In",
                          style: Theme.of(context).textTheme.headline1,
                        ),
                        const SizedBox(height: 50),
                        buildEmail(),
                        const SizedBox(height: 20),
                        buildPassword(),
                        const SizedBox(height: 5),
                        buildForgotPassBtn(),
                        buildRememberCb(),
                        buildLoginBTN(),
                        buildSignUpBTN()
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
