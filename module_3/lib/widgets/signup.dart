import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:module_3/widgets/main_content.dart';
import 'package:module_3/widgets/login.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

/*The state of the SignIn page*/
class _SignUpPageState extends State<SignUpPage> {
  bool isRememberMe = false;

/*Building first name widget*/
  Widget buildFName() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("First Name", style: Theme.of(context).textTheme.headline2),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.name,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.person_outline,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "e.g. Johannes",
              hintStyle: TextStyle(
                color: Theme.of(context).hintColor,
              ),
            ),
          ),
        )
      ],
    );
  }

/*Building alias widget*/
  Widget buildLName() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Surname",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.name,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.person,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "e.g. Doe",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

// /*Building alias widget*/
//   Widget buildAlias() {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         Text(
//           "Brand Name",
//           style: TextStyle(
//               color: Colors.white70, fontSize: 16, fontWeight: FontWeight.bold),
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Container(
//           alignment: Alignment.centerLeft,
//           decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.circular(20),
//               boxShadow: [
//                 BoxShadow(
//                     color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
//               ]),
//           height: 50,
//           child: TextField(
//             keyboardType: TextInputType.name,
//             style: TextStyle(color: Colors.black87),
//             decoration: InputDecoration(
//                 border: InputBorder.none,
//                 contentPadding: EdgeInsets.only(top: 14),
//                 prefixIcon: Icon(
//                   Icons.person,
//                   color: Color(0xff5ac18e),
//                 ),
//                 hintText: "Your name or business name",
//                 hintStyle: TextStyle(color: Colors.black38)),
//           ),
//         )
//       ],
//     );
//   }

  /*Building email widget*/
  Widget buildEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Email Address",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.email,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "e.g. email@example.com",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

/*Building school widget*/
  Widget buildInstitution() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Educational Institution Name",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.name,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.school,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "e.g. North West University",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

/*Building city/town widget*/
  Widget buildResidence() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "City/Town Name",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            keyboardType: TextInputType.name,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.location_city,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "e.g. Potchefstroom",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

/*Building password widget*/
  Widget buildPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Password",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.lock_outline,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "Password",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

/*Building confirm password widget*/
  Widget buildPasswordConfirm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Confirm Password",
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 50,
          child: TextField(
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: const EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.lock,
                color: Theme.of(context).iconTheme.color,
              ),
              hintText: "Type password again",
              hintStyle: TextStyle(color: Theme.of(context).hintColor),
            ),
          ),
        )
      ],
    );
  }

  ///Ts and Cs, and privacy check widget
  Widget buildTsCs() {
    return Container(
      height: 20,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: isRememberMe,
              checkColor: Theme.of(context).iconTheme.color,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  isRememberMe = value!;
                });
              },
            ),
          ),
          Row(
            children: [
              Text(
                "Do you accept the Ts & Cs?",
                style: Theme.of(context).textTheme.headline2,
              ),
            ],
          )
        ],
      ),
    );
  }

  /* Building the privacy policy link */
  /// Privacy policy widget
  Widget buildPrivacyPolicyButton() {
    return GestureDetector(
      onTap: () => {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUpPage()))
      },
      child: RichText(
        textAlign: TextAlign.justify,
        text: TextSpan(children: [
          TextSpan(
              text: "Read our ", style: Theme.of(context).textTheme.headline2),
          TextSpan(
              text: "Privacy policy.",
              style: Theme.of(context).textTheme.headline2),
        ]),
      ),
    );
  }

  Widget buildLoginBtn() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 25),
      width: double.infinity,
      child: ElevatedButton(
        // elevation: 5,
        onPressed: () => print("Login button pressed!"),
        // padding: EdgeInsets.all(15),
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        //color: Colors.white,
        child: Text("Login",
            style: TextStyle(
                color: Color(0xff5a1c8e),
                fontSize: 18,
                fontWeight: FontWeight.bold)),
      ),
    );
  }

  /*Building sign up button*/
  Widget buildJustText() {
    return GestureDetector(
        onTap: () => print("Sign up presses!!"),
        child: const Text(
          "Welcome to SMARKET",
          style: TextStyle(
              color: Colors.white54, fontSize: 20, fontWeight: FontWeight.bold),
        ));
  }

/*Building create account button*/
  Widget buildCreateAccountBTN() {
    return Container(
      padding: const EdgeInsets.only(top: 35, bottom: 10),
      width: 300,
      child: ElevatedButton(
        onPressed: () => {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => const MainPage()))
        },
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Text(
            "Create Account",
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        style: ButtonStyle(
          backgroundColor:
              MaterialStateProperty.resolveWith((Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed)) {
              return Theme.of(context).primaryColor;
            }
            return Theme.of(context).canvasColor;
          }),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(28),
                  bottomRight: Radius.circular(28),
                  bottomLeft: Radius.circular(28),
                  topLeft: Radius.circular(28)),
              side: BorderSide(
                color: Colors.grey[700]!,
              ),
            ),
          ), //
        ),
      ),
    );
  }

/*Building "already have an account widget?" button*/
  /// "Already have an account?" widget
  Widget buildAlreadyHaveAnAccount() {
    return GestureDetector(
      onTap: () => {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignInPage()))
      },
      child: RichText(
        text: TextSpan(children: [
          TextSpan(
              text: "Already have an Account? ",
              style: Theme.of(context).textTheme.headline6),
          TextSpan(text: "Login", style: Theme.of(context).textTheme.headline2),
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: const [
                      0.1,
                      0.9
                    ],
                        colors: [
                      Theme.of(context).scaffoldBackgroundColor,
                      Theme.of(context).primaryColor
                    ])),
                child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding:
                        /*EdgeInsets.symmetric(horizontal: 25, vertical: 120),
                        */
                        const EdgeInsets.fromLTRB(30, 120, 30, 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Create Account",
                          style: Theme.of(context).textTheme.headline1,
                        ),
                        const SizedBox(height: 50),
                        buildFName(),
                        const SizedBox(height: 20),
                        buildLName(),
                        const SizedBox(height: 20),
                        /*buildAlias(),
                        SizedBox(height: 20),*/
                        buildEmail(),
                        const SizedBox(height: 20),
                        buildInstitution(),
                        const SizedBox(height: 20),
                        buildResidence(),
                        const SizedBox(height: 20),
                        buildPassword(),
                        const SizedBox(height: 20),
                        buildPasswordConfirm(),
                        const SizedBox(height: 20),
                        buildTsCs(),
                        buildPrivacyPolicyButton(),
                        buildCreateAccountBTN(),
                        buildAlreadyHaveAnAccount(),
                        const SizedBox(height: 50),
                        buildJustText(),
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
