// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:flutter/material.dart';
// ignore: unused_import
//import 'package:flutter_search_bar/flutter_search_bar.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Column(
        children: [buildSearchWidget(context), Text("Searched content la")],
      ),
    );
  }
}

/// Search box widget
Widget buildSearchWidget(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).shadowColor,
                    blurRadius: 6,
                    offset: const Offset(0, 2))
              ]),
          height: 60,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            style: Theme.of(context).textTheme.headline4,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.only(top: 14),
                prefixIcon: Icon(
                  Icons.search,
                  color: Theme.of(context).iconTheme.color,
                  size: 30,
                ),
                hintText: "Type to start search",
                hintStyle: TextStyle(color: Theme.of(context).hintColor)),
          ),
        )
      ],
    ),
  );
}

// Widget buildSearch() {
//   return Scaffold(
//     backgroundColor: Colors.grey,
//     //appBar: ,
//     body: Container(
//         padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
//         child:  //Text("Thsi is home"),
//         ),
//   );
// }

Widget searchWidget() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
            ]),
        height: 10,
        child: TextField(
          keyboardType: TextInputType.name,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.person_outline,
                color: Color(0xff5ac18e),
              ),
              hintText: "Type to search...",
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}
