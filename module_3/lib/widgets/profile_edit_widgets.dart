import 'package:flutter/material.dart';

class ProfileTextInput extends StatefulWidget {
  const ProfileTextInput({
    Key? key,
    required this.label,
    required this.text,
    required this.onChanged,
    required this.maxLines,
  }) : super(key: key);
  final String label;
  final String text;
  final ValueChanged<String> onChanged;
  final int maxLines;

  @override
  State<ProfileTextInput> createState() => _ProfileTextInputState();
}

class _ProfileTextInputState extends State<ProfileTextInput> {
  late final TextEditingController controller;

  /// Initialize the initial state
  @override
  void initState() {
    super.initState();

    controller = TextEditingController(text: widget.text);
  }

  /// Clean the controller
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20),
        Text(
          widget.label,
          style: Theme.of(context).textTheme.headline2,
        ),
        const SizedBox(height: 10),
        TextField(
          controller: controller,
          style: Theme.of(context).textTheme.headline5,
          maxLines: widget.maxLines,
          decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          ),
        ),
      ],
    );
  }
}
