import 'package:flutter/material.dart';

class SomePage extends StatelessWidget {
  const SomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: const Text("Some page that will do something. Please be patient!"),
    );
  }
}
