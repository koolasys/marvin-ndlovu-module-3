import 'package:flutter/material.dart';

import 'login.dart';
import 'signup.dart';

//Stateless widget for the startup page
class StartUpPage extends StatelessWidget {
  const StartUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Stack(
          children: <Widget>[
            Container(
              //padding: const EdgeInsets.fromLTRB(30, 120, 30, 10),
              height: double.infinity,
              //The BG container with gradient
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: const [
                      0.1,
                      0.9
                    ],
                    colors: [
                      Theme.of(context).scaffoldBackgroundColor,
                      Theme.of(context).primaryColor
                    ]),
              ),
            ),
            Column(
              //Column with all the widgets of the start up page
              children: <Widget>[
                // Container(
                //   child: Image(
                //     image: AssetImage('assets/logos/logo.png'),
                //   ), //Text("Logo"),
                //   decoration: BoxDecoration(
                //       //color: Colors.orange[700],
                //       //shape: BoxShape.circle,
                //       ),
                //   alignment: Alignment.center,
                //   //width: 300,
                //   height: 550,
                // ),
                const SizedBox(
                  height: 120,
                ),
                Container(
                  child: Text(
                    "Welcome to the SMARKET",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(
                    20,
                    70,
                    20,
                    20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: const Text(
                          "Get Started",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.white70,
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          //Horizonal row of buttons
                          children: <Widget>[
                            ElevatedButton(
                                onPressed: () => {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignInPage()))
                                    },
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Text(
                                    "Sign In",
                                    style:
                                        Theme.of(context).textTheme.headline3,
                                  ),
                                ),
                                style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith(
                                          (Set<MaterialState> states) {
                                    if (states
                                        .contains(MaterialState.pressed)) {
                                      return Theme.of(context).primaryColor;
                                    }
                                    return Theme.of(context).canvasColor;
                                  }),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: const BorderRadius.only(
                                              topLeft: Radius.circular(28),
                                              bottomLeft: Radius.circular(28)),
                                          side: BorderSide(
                                              color: Colors.grey[700]!))),
                                )),
                            ElevatedButton(
                              onPressed: () => {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignUpPage()))
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Text(
                                  "Sign Up",
                                  style: Theme.of(context).textTheme.headline3,
                                ),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed)) {
                                    return Theme.of(context).primaryColor;
                                  }
                                  return Theme.of(context).canvasColor;
                                }),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(28),
                                            bottomRight: Radius.circular(28)),
                                        side: BorderSide(
                                            color: Colors.grey[700]!))),
                              ), //
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
